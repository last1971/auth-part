<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use JetBrains\PhpStorm\ArrayShape;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    #[ArrayShape(['email' => "array", 'password' => "array"])] public function rules(): array
    {
        return [
            'email' => [
                'required',
                'email',
                function ($attribute, $value, $fail) {
                    $loginUser = User::query()->firstWhere('email', $value);
                    if (!$loginUser) {
                        $fail('The '.$attribute.' is invalid.');
                    }
                    $this->merge(compact('loginUser'));
                }
            ],
            'password' => [
                'string',
                'min:6',
                function ($attribute, $value, $fail) {
                    if ($this->loginUser && !Hash::check($value, $this->loginUser->password)) {
                        $fail('The '.$attribute.' is invalid.');
                    }
                },
            ]
        ];
    }
}
