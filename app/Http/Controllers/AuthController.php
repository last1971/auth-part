<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class AuthController extends Controller
{
    /**
     * @param LoginRequest $loginRequest
     * @return array
     */
    public function login(LoginRequest $loginRequest)
    {
        /** @var User $user */
        $user = $loginRequest->json('loginUser');
        return [
            'token' => $user
                ->createToken(
                    'login',
                    $user->getAllPermissions()->map(fn(Permission $permission) => $permission->name )->all()
                )
                ->plainTextToken
        ];
    }

    /**
     * @param Request $request
     * @return string[]
     */
    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        return ['token' => 'revoked'];
    }
}
