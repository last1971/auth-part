<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Gate;
use OpenApi\Annotations as OA;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return LengthAwarePaginator
     */
    public function index(): LengthAwarePaginator
    {
        Gate::authorize('admin-only');
        return User::query()->paginate();
    }

    /**
     * @OA\Post(
     * path="/user",
     * operationId="Register",
     * tags={"Register"},
     * summary="User Register",
     * description="User Register here",
     *     @OA\RequestBody(
     *         @OA\JsonContent(),
     *         @OA\MediaType(
     *            mediaType="appliaction/json",
     *            @OA\Schema(
     *               type="object",
     *               required={"name","email", "password", "password_confirmation"},
     *               @OA\Property(property="name", type="string", example="SomeUser"),
     *               @OA\Property(property="email", type="string", format="email"),
     *               @OA\Property(property="password", type="string", example="somepassword",description="minimum 6 characters"),
     *               @OA\Property(property="password_confirmation", type="string", example="somepassword")
     *            ),
     *        ),
     *    ),
     *      @OA\Response(
     *          response=201,
     *          description="Register Successfully",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=422,
     *          description="Unprocessable Entity",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     * )
     * Store a newly created resource in storage.
     *
     * @param UserCreateRequest $request
     * @return User
     */
    public function store(UserCreateRequest $request): User
    {
        $user = new User();
        $user->fill($request->validated());
        $user->save();
        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  User  $user
     * @return User
     */
    public function show(User $user): User
    {
        Gate::authorize('admin-yourself', $user);
        return $user;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdateRequest $request
     * @param User $user
     * @return User
     */
    public function update(UserUpdateRequest $request, User $user): User
    {
        Gate::authorize('admin-yourself', $user);
        $user->fill($request->validated());
        $user->save();
        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  User $user
     * @return JsonResponse
     */
    public function destroy(User $user)
    {
        Gate::authorize('admin-only');
        $user->delete();
        return response()->json(['message' => 'User was deleted']);
    }
}
