<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $test = User::query()->firstOrCreate(
            [
                'email' => 'test@test.test',
            ],
            [
                'password' => 'password',
                'name' => 'Test',
            ]
        );
        $test->assignRole('admin');
    }
}
