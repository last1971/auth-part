<?php

namespace Tests\Feature;

use App\Http\Requests\UserCreateRequest;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Validator;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @var User
     */
    private User $user;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->seed();
        $this->user = User::query()->create([
            'name' => 'User',
            'email' => 'user@user.user',
            'password' => 'password',
        ]);
    }

    /**
     * @return void
     */
    public function testIndex403(): void
    {
        $response = $this->actingAs($this->user)->get('/api/user');
        $response->assertForbidden();
    }

    /**
     * @return void
     */
    public function testIndex200(): void
    {
        $admin = User::query()->firstWhere('email', 'test@test.test');
        $response = $this->actingAs($admin)->get('/api/user');
        $response->assertOk();
        $response->assertJsonStructure([
            'total',
            'per_page',
            'data'
        ]);
    }

    /**
     * @return void
     */
    public function testStore201(): void
    {
        $response = $this->post(
            '/api/user',
            [
                'name' => 'user',
                'email' => 'new_user@user.user',
                'password' => '123456',
                'password_confirmation' => '123456'
            ],
        );
        $response->assertCreated();
    }

    /**
     * @dataProvider unvalidCreateUserRequestProvider
     * @param array $data
     * @return void
     */
    public function testStoreErrors(array $data): void
    {
        $request = new UserCreateRequest();
        $validator = Validator::make($data, $request->rules());
        $this->assertTrue($validator->fails());

    }

    /**
     * @return void
     */
    public function testShow200(): void
    {
        $admin = User::query()->firstWhere('email', 'test@test.test');
        $user = User::query()->create([
            'name' => 'user',
            'email' => 'new_user@user.user',
            'password' => '123456',
        ]);
        $response = $this->actingAs($admin)->get('api/user/' . $user->id);
        $response->assertOk();
        $response = $this->actingAs($user)->get('api/user/' . $user->id);
        $response->assertOk();
    }

    /**
     * @return void
     */
    public function testShow403(): void
    {
        $admin = User::query()->firstWhere('email', 'test@test.test');
        $user = User::query()->create([
            'name' => 'user',
            'email' => 'new_user@user.user',
            'password' => '123456',
        ]);
        $response = $this->actingAs($user)->get('api/user/' . $admin->id);
        $response->assertForbidden();
    }

    /**
     * @dataProvider validUpdateUserRequestProvider
     * @param array $data
     * @return void
     */
    public function testUpdate200(array $data): void
    {
        $admin = User::query()->firstWhere('email', 'test@test.test');
        $user = User::query()->create([
            'name' => 'user',
            'email' => 'new_user@user.user',
            'password' => '123456',
        ]);
        $response = $this->actingAs($admin)->putJson('api/user/' . $user->id, $data);
        $response->assertOk();
        $response = $this->actingAs($user)->putJson('api/user/' . $user->id, $data);
        $response->assertOk();
    }

    /**
     * @dataProvider unvalidUpdateUserRequestProvider
     * @param array $data
     * @return void
     */
    public function testUpdateErrors(array $data): void
    {
        $admin = User::query()->firstWhere('email', 'test@test.test');
        $user = User::query()->create([
            'name' => 'user',
            'email' => 'new_user@user.user',
            'password' => '123456',
        ]);
        $response = $this->actingAs($admin)->putJson('api/user/' . $user->id, $data);
        $response->assertStatus(422);
        $response = $this->actingAs($user)->putJson('api/user/' . $user->id, $data);
        $response->assertStatus(422);
    }

    /**
     * @return void
     */
    public function testDestroy200(): void
    {
        $admin = User::query()->firstWhere('email', 'test@test.test');
        $user = User::query()->create([
            'name' => 'user',
            'email' => 'new_user@user.user',
            'password' => '123456',
        ]);
        $response = $this->actingAs($admin)->delete('api/user/' . $user->id);
        $response->assertOk();
    }

    public function testDestroy403(): void
    {
        $user = User::query()->create([
            'name' => 'user',
            'email' => 'new_user@user.user',
            'password' => '123456',
        ]);
        $response = $this->actingAs($user)->delete('api/user/' . $user->id);
        $response->assertForbidden();
    }

    /**
     * @return string[][][]
     */
    public function unvalidCreateUserRequestProvider(): array
    {
        return [
            [[
                'name' => 'test',
                'email' => 'test',
                'password' => 'password',
            ]],
            [[
                'email' => 'test@test.ru',
                'password' => 'password',
            ]],
            [[
                'email' => 'test@test.test',
                'password' => 'pas',
            ]],
            [[
                'email' => 'test@test.test',
                'password' => 'passwo',
            ]],
            [[
                'name' => 'name',
                'email' => 'test@test.ru',
                'password' => 'password',
                'password_confirmation' => '123456',
            ]],
        ];
    }

    /**
     * @return string[][][]
     */
    public function validUpdateUserRequestProvider(): array
    {
        return [
            [[
                'name' => 'test',
            ]],
            [[
                'email' => 'test1@test.ru',
            ]],
            [[
                'password_confirmation' => 'pas123',
                'password' => 'pas123',
            ]],
        ];
    }

    /**
     * @return string[][][]
     */
    public function unvalidUpdateUserRequestProvider(): array
    {
        return [
            [[
                'name' => '',
            ]],
            [[
                'email' => 'test@test.test',
            ]],
            [[
                'password' => 'pas123',
            ]],
        ];
    }
}
