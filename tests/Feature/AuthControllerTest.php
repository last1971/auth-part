<?php

namespace Tests\Feature;

use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @var User
     */
    private User $user;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::query()->create([
            'name' => 'Test',
            'email' => 'test@test.test',
            'password' => 'password',
        ]);
    }

    /**
     * @return void
     */
    protected function tearDown(): void
    {
        parent::tearDown();
        unset($this->user);
    }

    /**
     * @return void
     */
    public function testLoginOk(): void
    {
        $response = $this->postJson('/api/login', [
            'email' => 'test@test.test',
            'password' => 'password',
        ]);
        $response->assertOk();
        $response->assertJsonStructure(['token']);
    }

    /**
     * @dataProvider unvalidDataProvider
     * @param array $data
     * @return void
     */
    public function testLoginUnvalidData(array $data): void
    {
        $request = new LoginRequest();
        $validator = Validator::make($data, $request->rules());
        $this->assertTrue($validator->fails());
    }

    /**
     * @return void
     */
    public function testLogout(): void
    {
        $response = $this->postJson('/api/login', [
            'email' => 'test@test.test',
            'password' => 'password',
        ]);
        $token = $response->json('token');
        $response = $this->withHeader('Authorization', 'Bearer ' . $token)->post('/api/logout');
        $response->assertOk();
        $this->assertEmpty($this->user->tokens()->get());
    }

    /**
     * @return string[][][]
     */
    public function unvalidDataProvider(): array
    {
        return [
            [[
                'email' => 'test',
                'password' => 'password',
            ]],
            [[
                'email' => 'test@test.ru',
                'password' => 'password',
            ]],
            [[
                'email' => 'test@test.test',
                'password' => 'pas',
            ]],
            [[
                'email' => 'test@test.test',
                'password' => 'passwo',
            ]],
        ];
    }
}
